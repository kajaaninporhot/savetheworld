﻿using UnityEngine;
using System.Collections;

public class RootNode : BaseNode {

    protected override BT_State Action()
    {
        return children[0].ExecuteNode();
    }
}

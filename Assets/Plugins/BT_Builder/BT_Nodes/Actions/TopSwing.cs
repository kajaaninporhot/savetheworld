﻿using UnityEngine;
using System.Collections;
using System;

public class TopSwing : BaseNode
{
    protected override void _init()
    {
        agent.Set<bool>(false, "top_swinging");
    }

    protected override void StartAction()
    {
        agent.Set<bool>(true, "top_swinging");
    }

    protected override BT_State Action()
    {
        if (agent.Get<bool>("top_swinging") == false)
            return BT_State.SUCCESS;
        else
            return BT_State.RUNNING;
    }
}

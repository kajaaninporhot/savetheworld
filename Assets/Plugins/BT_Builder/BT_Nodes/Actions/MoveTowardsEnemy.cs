﻿using UnityEngine;
using System.Collections;

public class MoveTowardsEnemy : BaseNode
{
    float running_duration;

    protected override void _init()
    {
        agent.Set<bool>(false, "near");
        agent.Set<bool>(false, "approach");
    }

    protected override void StartAction()
    {
        running_duration = 1.0f;
        agent.Set<bool>(true, "approach");
    }

    protected override BT_State Action()
    {
        if (agent.Get<bool>("near"))
            return BT_State.SUCCESS;
        else if (running_duration > 0)
        {
            running_duration -= Time.deltaTime;
            return BT_State.RUNNING;
        }
        else
            return BT_State.FAILURE;
    }

    protected override void EndAction()
    {
        agent.Set<bool>(false, "approach");
    }
}

﻿using UnityEngine;
using System.Collections;

public class LowSwing : BaseNode
{
    protected override void _init()
    {
        agent.Set<bool>(false, "low_swinging");
    }

    protected override void StartAction()
    {
        agent.Set<bool>(true, "low_swinging");
    }

    protected override BT_State Action()
    {
        if (agent.Get<bool>("low_swinging") == false)
            return BT_State.SUCCESS;
        else
            return BT_State.RUNNING;
    }
}

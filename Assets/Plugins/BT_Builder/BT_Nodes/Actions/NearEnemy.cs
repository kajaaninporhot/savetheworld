﻿using UnityEngine;
using System.Collections;

public class NearEnemy : BaseNode
{
    protected override void _init()
    {
        agent.Set<bool>(false, "near");
    }

    protected override BT_State Action()
    {
        if (agent.Get<bool>("near"))
            return BT_State.SUCCESS;
        else
            return BT_State.FAILURE;
    }
}

﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class DialogEditor : EditorWindow {

    string filename;
    string savepath;
    int folder;
    string[] folderList;
    string[] visibleFolderlist;     // HACK: Different path roots used all around code.
    BTeditor ed;

    bool emptyFilename = false;
    public static void Init(string filename, BTeditor editor, ref string[] subfolders, int currentFolder)
    {
        DialogEditor t = (DialogEditor)ScriptableObject.CreateInstance("DialogEditor");
        t.ShowUtility();
        t.filename = filename;
        t.folderList = subfolders;
        t.visibleFolderlist = new string[subfolders.Length];
        // So much hack. Removed default folderpath but kept "BT_Files".
        for (int i = 0; i < subfolders.Length; ++i )
        {
            t.visibleFolderlist[i] = subfolders[i].Substring(TreeData.xml_folderpath.Length - 8);
        }
        t.ed = editor;
        t.folder = currentFolder;
        t.titleContent = new GUIContent("Save file...");
    }
    void OnGUI()
    {
        if (emptyFilename)
            GUILayout.Label("File name cannot be empty! ");
        filename = EditorGUILayout.TextField("File name: ", filename);
        folder = EditorGUILayout.Popup("Folder: ", folder, visibleFolderlist);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            if (filename != null && filename != "")
            {
                savepath = folderList[folder] + "/" + filename;
                //Debug.Log(savepath);
                ed.SaveFile(savepath);
                //this.Close(); // Saving sets focus to BTeditor, so OnLostFocus gets called.
            }
            else emptyFilename = true;
        }
        if (GUILayout.Button("Cancel"))
            this.Close();
        GUILayout.EndHorizontal();
    }
    void OnLostFocus()
    {
        this.Close();
    }
}

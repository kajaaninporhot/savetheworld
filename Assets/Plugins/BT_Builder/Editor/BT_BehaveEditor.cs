﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


// Custom inspector view. The BT_Behave component is a manager class, which has nothing the user should edit.
[CustomEditor(typeof(BT_Behave))]
public class BT_BehaveEditor : Editor
{
    // This is only necessary for showing colors as actual colors 
    // instead of (or in addition to) a series of numbers.
    public void OnEnable()
    {
        BT_Behave manager = (BT_Behave)target;
        manager.blackBoard.color_initer();
    }
    public override void OnInspectorGUI()
    {
        BT_Behave manager = (BT_Behave)target;
        manager.show = EditorGUILayout.ToggleLeft("Show Blackboard contents", manager.show);
        if (manager.show)
        {
            // Basic blackboard which exists in Behave component itself.
            foreach (KeyValuePair<string, object> item in manager.memory)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(item.Key);
                if (item.Value != null)
                    GUILayout.Label(item.Value.ToString());
                else GUILayout.Label("-no value-");
                GUILayout.EndHorizontal();
            }
            // Type specific blackboards on separate class.
            manager.blackBoard.ReadInts();
            manager.blackBoard.ReadFloats();
            manager.blackBoard.ReadBools();
            manager.blackBoard.ReadDoubles();
            manager.blackBoard.ReadStrings();
            manager.blackBoard.ReadVector2s();
            manager.blackBoard.ReadVector3s();
            manager.blackBoard.ReadQuaternions();
            manager.blackBoard.ReadColors();
        }
        // NB! This is (or will be) deprecated. TODO: Workaround.
        EditorUtility.SetDirty(manager);
    }
}

﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

// Invalid nodes have no actual class implementation or class reference is unclear.
public enum NODETYPES
{
    ROOT,
    INTERNAL,
    DECORATOR,
    LEAF,
    INVALID
}
////////////////////////////////////////////////////////////////////////
/*
 * TODO:
 * Check compatibility!
 * Add graphics & design layout.
 *      Visual for subtree root!
 *      Invalid node drawing logic
 *      
 * Dialog popups for saving/importing (ModalWindow doesn't work in editor)
 * 
 * 
 * Wrapping & scrolling for notes (if several lines are visible)
 *      Also minimize-button in the note window itself (-), (i) -button is not clear enough
 * Clean code up!!
 * (User-defined xml folder path! Editor/BT_Files as default -> loading string splitting!) BUT:
 * NB! Check correct folder structure: 
 * (http://forum.unity3d.com/threads/where-to-install-your-assets-for-scripting-packages-editor-extensions.292674/)
 */
 
////////////////////////////////////////////////////////////////////////

// node info.
public class node
{
    public string       name;
    public Rect         windowRect;
    public int          uniqueID;        // index may change. This is used for drawing! Each window needs ID.
    public bool         isDead;
    public int          treeLevel;
    public List<node>   children;
    public float        spaceNeeded;     // For automatic editor layout (snapping)
    public NODETYPES    nodetype;
    public bool         isVisible;       // For LOD modifications
    public string       notes;           // For user custom notes
    public bool         showNotes;       // For showing said notes
    public Rect         noteWindow;
    public Rect         noteRect;
    public int          noteID;          // For window drawing.
    public string       subtree_name;    // Used when node is root of imported subtree. Saved as notes (added).

                                         
    public List<exportedVariable> vars;
    
    public node() { }
    public node(NODETYPES type, string _name, int id, int noteId, float posX, float posY, float width, float height)
    {
        name        = _name;
        windowRect  = new Rect(posX, posY, width, height);
        uniqueID    = id;
        isDead      = false;
        treeLevel   = 0;
        children    = new List<node>();
        spaceNeeded = 70;
        nodetype    = type;
        isVisible   = true;
        notes       = "";
        showNotes   = false;
        noteWindow  = new Rect(windowRect.x + 10, windowRect.y + 50, 120, 60);
        noteRect    = new Rect(2, 17, 116, 41);
        noteID      = noteId;
        vars        = new List<exportedVariable>();

        // Find all class variables (properties!) and set them into vars list. Not used for invalid types.
        if (nodetype != NODETYPES.INVALID)
        {
            System.Type t = TreeData.getType(_name);  //System.Type.GetType(_name); // Editor project cannot access main project classes directly.
            PropertyInfo[] variables = t.GetProperties(BindingFlags.Public | 
													   BindingFlags.NonPublic | 
													   BindingFlags.Instance | 
													   BindingFlags.FlattenHierarchy);
            if (variables.Length != 0)
            {
                foreach (PropertyInfo p in variables)
                {
                    {
                        exportedVariable addthis = new exportedVariable();
                        addthis.name = p.Name;
                        System.Type m = p.PropertyType;
                        addthis.type = m.ToString();
                        if (m.IsValueType)
                            addthis.value = System.Activator.CreateInstance(m);
                        vars.Add(addthis);
                    }
                }
            }
        }
    }
}
/// <summary>
/// Custom editor class starts here.
/// </summary>
public class BTeditor : EditorWindow
{
    // Counter for unique node IDs. TODO: Replace with more secure system if this is too error-prone.
    public int idi = 0;
    
    // Node type lists. Contents are retrieved from corresponding folders. File name must be the same as class name,
    // since it is used in tree initialisation.
    string[] internaltypes;
    string[] actiontypes; 
    string[] conditiontypes;
    string[] decoratortypes;

    // Paths to main folders of each selection button. These may or may not change.
    string xmlfolderpath        = "Assets/Plugins/BT_Builder/BT_Files";
    string internalsfolderpath  = "Assets/Plugins/BT_Builder/BT_Nodes/Internals";
    string actionsfolderpath    = "Assets/Plugins/BT_Builder/BT_Nodes/Actions";
    string conditionsfolderpath = "Assets/Plugins/BT_Builder/BT_Nodes/Conditions";
    string decoratorsfolderpath = "Assets/Plugins/BT_Builder/BT_Nodes/Decorators";

    // Saved XML files list.
    string[] treeFiles;
    string[] tree_subfolders;

    // Rectangles for editor controls.
    const float bW  = 150;    // ButtonWidth
    const float bH  = 30;     // ButtonHeight
    const float lH  = 20;     // LabelHeight
    const float rH  = 70;     // RowHeight     for short.

    // First row

    Rect SnapToHierarchy_Button   = new Rect(bW * 0,  0, bW, bH);
    Rect TreeDepth_Field          = new Rect(bW * 0, bH, bW, bH);
    Rect Save_Button              = new Rect(bW * 2,  0, bW, bH);
    Rect SaveName_TextField       = new Rect(bW * 2, bH, bW, lH);
    Rect SaveByName_Button        = new Rect(bW * 3,  0, bW, bH);    
    Rect ImportTree_Button        = new Rect(bW * 4,  0, bW, bH);
    Rect ImportSubTree_Toggle     = new Rect(bW * 4, bH, bW, lH);
    Rect TreeValidity_Field       = new Rect(bW * 1,  0, bW, bH);
    Rect TreeValidity_Field2      = new Rect(bW * 1, bH, bW, bH);
    Rect Clear_Button             = new Rect(bW * 5,  0, bW, bH);
    
    // Second row
    Rect CreateNode_Button        = new Rect(bW * 0, rH, bW, bH);
    Rect CreateCondition_Button   = new Rect(bW * 1, rH, bW, bH);
    Rect CreateAction_Button      = new Rect(bW * 2, rH, bW, bH);
    Rect CreateDecorator_Button   = new Rect(bW * 3, rH, bW, bH);

    // General node specifics
    const float nodePosX   = 0;
    const float nodePosY   = 80;
    const float nodeWidth  = 130;
    const float nodeHeight = 70;

    // Used as default position for new node. 
    Rect Close_button               = new Rect(nodeWidth - 16,  0, 16, 16);
    Rect AddConnection_button_left  = new Rect( 0, 20, 25, 25);
    Rect AddConnection_button_right = new Rect(nodeWidth - 25, 20, 25, 25);
    Rect ShowNotes_Button           = new Rect(0, nodeHeight - 20, 20, 20);
    Rect Expand_Button              = new Rect(nodeWidth - 20, nodeHeight -17, 20, 17);
    Rect ShowVars_Button            = new Rect(40, 20, 50, 20);

    // Bezier tangent points
    Vector2 rightOffset = new Vector2(nodeWidth/2, 0);
    Vector2 leftOffset  = new Vector2(-nodeWidth/2, 0);

    // editor control vars
    bool importAsSubtree    = false;
    bool createclick        = false;                    // if a node selecting menu should be open.
    float messageTimer      = 2;                        // How long the "saved tree" message should be shown.
    int treeDepth           = 0;                        // Root is level 0.
    int controlArea_height  = 120;
    const float nodeOffsetX = nodeWidth + 60;           // Automatic layout offsets
    const float nodeOffsetY = nodeHeight + 10;
    string filename         = null;                     // Name of xml file to save.
    int currentfolder       = 0;                        // Index of folder to save the xml in.
    string currentpath      = null;
    BTree activeTree        = null;                     // Reference to currently active BTree component. 
                                                        // If tree editor is opened from the component, file is assigned to it on save.
    float messageCreated    = -5;                        // Creating time for "saved" message.

    // editor color vars. Set from init to match current skin
    Color controlAreaBackground;
    Color controlAreaSeparator;

    // window scrolling
    Vector2 scrollPosition;
    float maxNodeX, maxNodeY;

    // helpers for WindowFunction
    node cuurNode;                          // temp variable to store currently drawn node window in
    bool connectingRight, connectingLeft;   // Mark connecting nodes in progress
    node startpoint;                        // temp variable for connecting nodes

    // Graphics for WindowFunction
    Texture2D invalidNode;

    // containers for tree structure
    List<node> nodes = new List<node>();    
    
    [MenuItem("Window/Behavior Tree Editor")]
    static void Init()
    {
        BTeditor e = (BTeditor)EditorWindow.GetWindow(typeof(BTeditor));
        e.InitBT(null);
    }
    public void InitBT(BTree tree)
    {
        idi = 0;
        int id = GetUniqueId();
        int nid = GetUniqueId();
        titleContent = new GUIContent("new behavior");
        nodes.Add(new node(NODETYPES.ROOT, "RootNode", id, nid, 0,200, nodeWidth, nodeHeight));
        if (!AssetDatabase.IsValidFolder("Assets/" +TreeData.xml_folderpath))
        {
            AssetDatabase.CreateFolder("Assets/Plugins/BT_Builder", "BT_Files");
        }
        GetXMLFiles(xmlfolderpath, ref treeFiles, ref tree_subfolders);
        GetFiles(internalsfolderpath, ref internaltypes);
        GetFiles(actionsfolderpath, ref actiontypes);
        GetFiles(conditionsfolderpath, ref conditiontypes);
        GetFiles(decoratorsfolderpath, ref decoratortypes);
        activeTree = tree;
        invalidNode = (Texture2D)EditorGUIUtility.Load("BT_Builder/invalidNode.png");
        if (EditorGUIUtility.isProSkin)
        {
            controlAreaBackground = new Color(0.24f, 0.24f, 0.24f, 1);
            controlAreaSeparator  = new Color(0.1f, 0.1f, 0.1f, 1);
        }
        else
        {
            controlAreaBackground = new Color(0.85f, 0.85f, 0.85f, 1);
            controlAreaSeparator = new Color(0.55f, 0.55f, 0.55f, 1);
        }
    }
    private void OnGUI()
    {
        // slightly tinted background. Also darker line to separate control area from tree window.
        EditorGUI.DrawRect(new Rect(0, 0, position.width, controlArea_height), controlAreaBackground);
        EditorGUI.DrawRect(new Rect(0, controlArea_height-1, position.width, 1), controlAreaSeparator);
		
        // Editor tools
        CreateNodeButton();
        CreateActionButton();
        CreateConditionButton();
        CreateDecoratorButton();
        HierarchyButton();
        SaveToXMLButton();
        SaveByNameButton();
        ImportButton();
        SubtreeToggle();
        ClearButton();

        // Tool row info panels. Check if initialization has happened.
        if (nodes.Count > 0)
        {
            EditorGUI.LabelField(TreeDepth_Field, "Tree Depth: " + treeDepth.ToString());
            TreeValidityMessage();
            TreeSavedMessage(); // Meh with this.
        }
		else nodes.Add(new node(NODETYPES.ROOT, 
                                "RootNode", 
                                GetUniqueId(), 
                                GetUniqueId(), 
                                0, 
                                200, 
                                nodeWidth, 
                                nodeHeight));

        // scrolling tree area begins here
        scrollPosition = GUI.BeginScrollView(new Rect(0, 
                                                      controlArea_height, 
                                                      position.width, 
                                                      position.height-controlArea_height), 
                                             scrollPosition, 
                                             new Rect(0, 
                                                      controlArea_height, 
                                                      maxNodeX, 
                                                      maxNodeY-controlArea_height));        
        
        // Draw connecting bezier lines between nodes
        Handles.BeginGUI();

        DrawConnections();

        Handles.EndGUI();

        maxNodeX = 200;
        maxNodeY = 200;
        // Draw nodes
        BeginWindows();
        foreach (node n in nodes)
        {
            if (n.isVisible)
            {
                if (n.showNotes)
                    n.noteWindow = GUI.Window(n.noteID, n.noteWindow, NoteWindowFunction, "notes");
                n.windowRect = GUI.Window(n.uniqueID, n.windowRect, WindowFunction, n.name);
            }
        }
        EndWindows();
        GUI.EndScrollView();
		
        ////////////////////////////////////////////////////////////  End of window drawing

        // Destroy destroyed nodes
        for (int i = nodes.Count -1; i >= 0; i--)
        {
            if (nodes[i].isDead)
                nodes.RemoveAt(i);
        }
    }

    // Node window functionality.
    void WindowFunction(int windowID)
    {
        int index = FindNodeWithId(windowID);
        if (index >= 0)
        {
            cuurNode = nodes[index];

            // Check window boundaries: area cannot be expanded to left / top
            if (cuurNode.windowRect.x < 0)
                cuurNode.windowRect.x = 0;
            if (cuurNode.windowRect.y < controlArea_height)
                cuurNode.windowRect.y = controlArea_height;

            // Update extremes of currently used area
            int extraSpace = 10;
            if (cuurNode.showNotes) extraSpace += 50;
            if (cuurNode.windowRect.xMax > maxNodeX)
                maxNodeX = cuurNode.windowRect.xMax + 10;
            if (cuurNode.windowRect.yMax > maxNodeY)
                maxNodeY = cuurNode.windowRect.yMax + extraSpace;

            // Update notes position
            cuurNode.noteWindow.x = cuurNode.windowRect.x + 10;
            cuurNode.noteWindow.y = cuurNode.windowRect.y + 65;

            // Draw invalid node texture if invalid
            if (cuurNode.nodetype == NODETYPES.INVALID)
            { 
                GUI.DrawTexture(new Rect(3, 17, nodeWidth - 6, nodeHeight - 20), invalidNode, ScaleMode.StretchToFill);
            }

            // Show/hide notes toggle button
            if (GUI.Button(ShowNotes_Button, "i"))
                cuurNode.showNotes = !cuurNode.showNotes;

            // TODO: Should this be var in node data?
            bool hidden = (cuurNode.children.Count != 0 && !cuurNode.children[0].isVisible);
            // Show/hide subtree toggle button
            if (cuurNode.children.Count != 0)
            {
                if (GUI.Button(Expand_Button, "E"))
                {
                    SetVisibilityOfChildren(cuurNode, hidden);
                    if (hidden)
                    {
                        parentspace(FindRoot());
                        AdjustRootPosition();
                        HierarchyLayout(FindRoot());
                    }
                }
            }

            // Imported subtree name label
            GUI.Label(new Rect(20, nodeHeight - 20, nodeWidth - 40, 20), cuurNode.subtree_name);

            // Right connecting button (connect to child)
            if (cuurNode.nodetype != NODETYPES.LEAF && !hidden)
            {
                if (connectingLeft && startpoint == cuurNode)
                GUI.color = Color.green;
                if (GUI.Button(AddConnection_button_right, ">"))
                {
                    if (connectingLeft)
                    {
                        connectingLeft = false;
                        ConnectNodes(cuurNode, startpoint);
                    }
                    else if (connectingRight)
                    {
                        connectingRight = false;
                    }
                    else if (!connectingRight)
                    {
                        connectingRight = true;
                        startpoint = cuurNode;
                    }
                    else { }
                }
            }
            // Left connecting button (connect to parent)
            if (cuurNode.nodetype != NODETYPES.ROOT)
            {
                if (GUI.Button(AddConnection_button_left, "<"))
                {
                    if (connectingRight)
                    {
                        connectingRight = false;
                        ConnectNodes(startpoint, cuurNode);
                    }
                    else if (connectingLeft)
                    {
                        connectingLeft = false;
                    }
                    else if (!connectingLeft)
                    {
                        connectingLeft = true;
                        startpoint = cuurNode;
                    }
                    else { }
                }
                
            }
            // Show variable pop up button
            if (cuurNode.nodetype != NODETYPES.ROOT 
                && cuurNode.nodetype != NODETYPES.INVALID 
                && cuurNode.vars.Count > 0)
            { 
                if (GUI.Button(ShowVars_Button, "Edit"))
                {
                    ValueEditor.Init(ref cuurNode.vars);
                }
            }
            // Node deleting button
            if (cuurNode.nodetype != NODETYPES.ROOT)
            {
                if (GUI.Button(Close_button, "X"))
                {
                    KillNode(cuurNode);
                    KillConnections(cuurNode);
                    treeDepth = GetUpdatedTreeDepth(FindRoot(), 0);
                }
            }            
        }
        GUI.DragWindow();
    }

    // Message window function.
    void NoteWindowFunction(int windowID)
    {
        int index = FindNodeWithNoteId(windowID);
        if (index >= 0)
        {
            cuurNode = nodes[index];
            cuurNode.notes = EditorGUI.TextArea(cuurNode.noteRect, cuurNode.notes);
        }
    }	
	
    //////////////////////////////////////////////////
    //// Helpers
    //////////////////////////////////////////////////

    /// <summary>
    /// Organisers for finding files in project and arranging them to correct lists.
    /// </summary> 

    // Read node file names to given arrays.
    void GetFiles(string folderPath, ref string[] listToFill)
    {
        string[] folder = new string[] { folderPath };
        string[] temp = AssetDatabase.FindAssets("", folder);

        List<string> templist = new List<string>();
        // Populate.
        for (int i = 0; i < temp.Length; i++)
        {
            string fname = GetFullPath(temp[i], folderPath.Length);
            if (checkExtension(fname) != null)
            {
                // Check if name represents a class that inherits from BaseNode
                string strippedName = removeExtension(fname);
                if (CheckIfNode(strippedName))
                {
                    templist.Add(strippedName);
                }
            }
        }
        listToFill = new string[templist.Count];
        templist.CopyTo(listToFill);
    }
    // different system for xml files. Also saves folders for saving.
    void GetXMLFiles(string folderPath, ref string[] listToFill, ref string[] subfolderlist)
    {
        string[] folder = new string[] { folderPath };
        string[] temp = AssetDatabase.FindAssets("", folder);

        List<string> templist = new List<string>();
        List<string> tempfolderlist = new List<string>();

        // Folder list has paths to all existing folders that are found by the editor.
        // BT_Files is the default folder. Add this to all paths?
        tempfolderlist.Add(TreeData.xml_folderpath);

        // Populate lists:
        for (int i = 0; i < temp.Length; i++)
        {
            string fname = GetFullPath(temp[i], folderPath.Length);
            Debug.Log(fname);
            string ext = checkExtension(fname);
            if (ext == "xml")
                templist.Add(removeExtension(fname));
            // Add folder to available folders list. Add BT_Files to name?
            else if (ext == null)
            {
                string fullPath = TreeData.xml_folderpath + "/" + fname;
                if (!tempfolderlist.Contains(fullPath))
                    tempfolderlist.Add(fullPath);
            }
        }
        // set lists to arrays used by the editor:
        listToFill = new string[templist.Count];
        templist.CopyTo(listToFill);
        subfolderlist = new string[tempfolderlist.Count];
        tempfolderlist.CopyTo(subfolderlist);
    }

    /// <summary>
    /// Editor control buttons
    /// </summary>
    void DrawConnections()
    {
        foreach (node n in nodes)
        {
            if (n.children.Count > 0 && n.children[0].isVisible)
            {
                foreach (node child in n.children)
                {
                    Handles.DrawBezier(n.windowRect.center + rightOffset,
                                       child.windowRect.center + leftOffset,
                                       new Vector2(n.windowRect.xMax + 20f, n.windowRect.center.y),
                                       new Vector2(child.windowRect.x - 20f, child.windowRect.center.y),
                                       Color.blue,
                                       null,
                                       5f);
                }
            }
        }
    }

    void HierarchyButton()
    {
        if (GUI.Button(SnapToHierarchy_Button, "Snap to grid"))
        {
            // priority by visual positioning
            foreach (node n in nodes)
            {
                if (n.children.Count > 1)
                {
                    updatePriority(n);
                }
            }
            // snapping
            parentspace(FindRoot());
            AdjustRootPosition();
            HierarchyLayout(FindRoot()); 
        }
    }
    void TreeValidityMessage()
    {
        if (!EndsInLeaf())
            EditorGUI.LabelField(TreeValidity_Field, "Warning: Not all \n branches end in leaf");

        if (!ConnectsToRoot())
            EditorGUI.LabelField(TreeValidity_Field2, "Warning: Not all nodes \nare connected to root");
    }
    void TreeSavedMessage()
    {
        if (Time.realtimeSinceStartup < messageCreated + messageTimer)
        {
            EditorGUI.LabelField(SaveName_TextField, "Behavior saved.");
        }
    }
    void CreateNodeButton()
    {
        if (GUI.Button(CreateNode_Button, "Create new internal..."))
        {
            createclick = !createclick;
        }
        if (createclick)
        {
            GenericMenu gen = new GenericMenu();
            foreach (string s in internaltypes)
            {
                gen.AddItem(new GUIContent(s), false, CreateNodeFunc, s);
            }
            gen.DropDown(CreateNode_Button);

            if (!CreateNode_Button.Contains(Input.mousePosition))
                createclick = false;
        }
    }
    void CreateActionButton()
    {
        if (GUI.Button(CreateAction_Button, "Create new action..."))
        {
            createclick = !createclick;
        }
        if (createclick)
        {
            GenericMenu gen = new GenericMenu();
            foreach (string s in actiontypes)
            {
                gen.AddItem(new GUIContent(s), false, CreateNodeFunc, s);
            }
            gen.DropDown(CreateAction_Button);

            if (!CreateAction_Button.Contains(Input.mousePosition))
                createclick = false;
        }
    }
    void CreateConditionButton()
    {
        if (GUI.Button(CreateCondition_Button, "Create new condition..."))
        {
            createclick = !createclick;
        }
        if (createclick)
        {
            GenericMenu gen = new GenericMenu();
            foreach (string s in conditiontypes)
            {
                gen.AddItem(new GUIContent(s), false, CreateNodeFunc, s);
            }
            gen.DropDown(CreateCondition_Button);

            if (!CreateCondition_Button.Contains(Input.mousePosition))
                createclick = false;
        }
    }
    void CreateDecoratorButton()
    {
        if (GUI.Button(CreateDecorator_Button, "Create new decorator..."))
        {
            createclick = !createclick;
        }
        if (createclick)
        {
            GenericMenu gen = new GenericMenu();
            foreach (string s in decoratortypes)
            {
                gen.AddItem(new GUIContent(s), false, CreateNodeFunc, s);
            }
            gen.DropDown(CreateDecorator_Button);

            if (!CreateDecorator_Button.Contains(Input.mousePosition))
                createclick = false;
        }
    }
    // Callback for node creating dropdown menus
    void CreateNodeFunc(object name)
    {
        string _name = (string)name;
        CreateNode(_name, GetType(_name));
        createclick = false;
    }
    void ClearButton()
    {
        if (GUI.Button(Clear_Button, "New Behavior..."))
        {
            nodes.Clear();
            node root = new node(NODETYPES.ROOT, "RootNode", GetUniqueId(), GetUniqueId(), 0, 200, nodeWidth, nodeHeight);
            nodes.Add(root);
            treeDepth = GetUpdatedTreeDepth(root, 0);
            filename = null;
            this.titleContent = new GUIContent("new behavior");
        }
    }
    void SaveToXMLButton()
    {
        if (GUI.Button(Save_Button, "Save Tree"))
        {
            // Update priority by visual positioning
            foreach (node n in nodes)
            {
                if (n.children.Count > 1)
                {
                    updatePriority(n);
                }
            }
            // Open dialog only if no name is given
            if (filename == null || filename == "")
            {
                DialogEditor.Init(filename, this, ref tree_subfolders, currentfolder);
            }
            else SaveFile(currentpath);
        }
    }
    void SaveByNameButton()
    {
        if (GUI.Button(SaveByName_Button, "Save Tree as..."))
        {
            // Update priority by visual positioning
            foreach (node n in nodes)
            {
                if (n.children.Count > 1)
                {
                    updatePriority(n);
                }
            }
            DialogEditor.Init(filename, this, ref tree_subfolders, currentfolder);
        }
    }
    public void SaveFile(string fullPathFromAssets)
    {
        TreeData data = new TreeData();
        data.RootNode = Convert(FindRoot());
        string assetPath = fullPathFromAssets + ".xml";
        filename = GetNameWithoutFolders(fullPathFromAssets);

        data.Save(Path.Combine(Application.dataPath, assetPath));

        AssetDatabase.Refresh();

        // Update file list
        GetXMLFiles("Assets/" + TreeData.xml_folderpath, ref treeFiles, ref tree_subfolders);

        if (activeTree != null)
            AssignToCurrentTree(assetPath);
        messageCreated = Time.realtimeSinceStartup;
        this.titleContent = new GUIContent(filename);
        this.Focus();
    }
    void AssignToCurrentTree(string assetPath)
    {
        // if BTree is active, assign saved file to it
        if (activeTree != null)
        {
            TextAsset te = (TextAsset)AssetDatabase.LoadAssetAtPath("Assets/" + assetPath, typeof(TextAsset));
            activeTree.SetTreeFile(te);
        }
    }

    void ImportButton()
    {
        if (GUI.Button(ImportTree_Button, "Open existing..."))
        {
            createclick = !createclick;
        }
        if (createclick)
        {
            GenericMenu gen = new GenericMenu();
            foreach (string s in treeFiles)
            {
                gen.AddItem(new GUIContent(s), false, ImportXMLFunc, s);
            }
            gen.DropDown(ImportTree_Button);

            if (!ImportTree_Button.Contains(Input.mousePosition))
                createclick = false;
        }
    }
    void ImportXMLFunc(object file)
    {
        string name = (string)file;
        OpenTreeForEditing(name, importAsSubtree);
    }
    public void OpenTreeForEditing(string filepath, bool isSubtree)
    {
        // filename should have no folder path attached.
        string fnam = GetNameWithoutFolders(filepath);
        // Save the full path relative to Assets folder
        currentpath = TreeData.xml_folderpath + "/" + filepath;
        TreeData data;
        {
            data = TreeData.Load(Path.Combine(Application.dataPath, currentpath + ".xml"));
            if (isSubtree)
            {
                node n = ConvertBack(data.RootNode.children[0]);
                SetVisibilityOfChildren(n, false);
                n.subtree_name = fnam;
            }
            else
            {
                nodes.Clear();
                ConvertBack(data.RootNode);
                treeDepth = GetUpdatedTreeDepth(FindRoot(), 0);
                parentspace(FindRoot());
                AdjustRootPosition();
                HierarchyLayout(FindRoot());
                filename = fnam;
                this.titleContent = new GUIContent(fnam);
                // HACK-like way to extract folder name. File name and folder should be defined separately.
                currentfolder = FindFolderIndex(currentpath.Substring(0, currentpath.Length - (fnam.Length + 1)));
            }
        }
    }
    int FindFolderIndex(string filepath)
    {
        for (int i = 0; i < tree_subfolders.Length; ++i)
        {
            if (tree_subfolders[i].Contains(filepath))
                return i;
        }
        // Use main folder as default. TODO: See if this is wrong.
        return 0;
    }
    void SubtreeToggle()
    {
        importAsSubtree = GUI.Toggle(ImportSubTree_Toggle, importAsSubtree, "Import as subtree");
    }

    /// <summary>
    /// Importing / exporting data conversions.
    /// </summary>
    expNode Convert(node root)
    {
        expNode node = new expNode();
        node.name = root.name;
        node.notes = root.subtree_name + "\n" + root.notes;
        node.isExpanded = root.isVisible;
        if (root.vars.Count != 0)
        {
            foreach (exportedVariable v in root.vars)
            {                
                if (v.type != "UnityEngine.Color" && 
					v.type != "UnityEngine.Vector2" && 
					v.type != "UnityEngine.Vector3" && 
					v.type != "UnityEngine.Rect")
					
                    node.vars.Add(v);
                else 
                {
                    BT_Serializer bt = new BT_Serializer();
                    node.vars.Add(bt.ConvertToSerializable(v));
                }
            }
        }
        if (root.children.Count != 0)
        {
            foreach (node n in root.children)
                node.children.Add(Convert(n));
        }
        return node;
    }
    node ConvertBack(expNode root)
    {
        node node = new node(GetType(root.name), 
							 root.name, 
							 GetUniqueId(), 
							 GetUniqueId(), 
							 scrollPosition.x, 
							 scrollPosition.y + controlArea_height, 
							 nodeWidth, 
							 nodeHeight);
        node.notes = root.notes;
        node.isVisible = root.isExpanded;
        nodes.Add(node);
        if (root.vars.Count != 0)
        {
            foreach (exportedVariable n in root.vars)
            {
                for (int i = 0; i < node.vars.Count; ++i )
                {
                    if (n.name == node.vars[i].name)
                    {
                        if (n.type != "UnityEngine.Color" && 
							n.type != "UnityEngine.Vector2" && 
							n.type != "UnityEngine.Vector3" && 
							n.type != "UnityEngine.Rect")
                        {
                            node.vars[i].value = n.value;                            
                        }
                        else
                        {
                            BT_Serializer bt = new BT_Serializer();
                            node.vars[i].value = bt.ConvertToUnityType(n).value;
                        }
                        node.vars[i].type = n.type;
                    }                        
                }
            }
        }
        if (root.children.Count != 0)
        {
            foreach (expNode n in root.children)
                node.children.Add(ConvertBack(n));
        }

        return node;
    }
    string GetFileName(int pathLength, string guid)
    {
        string name = AssetDatabase.GUIDToAssetPath(guid);
        int length = pathLength + 1;

        name = name.Substring(length, name.Length - length);
        string[] n = name.Split('.');
        return n[0];
    }

    /// <summary>
    /// Helpers for finding and organising files in folders.
    /// </summary>
    /// 

    // Convert asset guid to path relative to the appropriate main folder. Extension included.
    string GetFullPath(string guid, int pathlength)
    {
        string name = AssetDatabase.GUIDToAssetPath(guid);
        // include the '/' to main path length:
        int length = pathlength + 1;
        name = name.Substring(length, name.Length - length);
        return name;
    }
    // Fairly HACK-like way to find out what we are dealing with here.
    // Check and return extension of the file. If there's no extension,
    // path refers to a folder and return value is null.
    string checkExtension(string fullPath)
    {
        string[] n = fullPath.Split('.');
        if (n.Length == 1)
            return null;
        return n[n.Length - 1];
    }
    // Remove extension from filepath
    string removeExtension(string fullPath)
    {
        string[] n = fullPath.Split('.');
        return n[0];
    }
    // Get asset name without folders:
    string GetNameWithoutFolders(string path)
    {
        string[] n = path.Split('/');
        return (n[n.Length - 1]);
    }
    // Check if given file name represents an existing class that inherits BaseNode
    // (and can be used in BT)
    bool CheckIfNode(string name)
    {
        if (TreeData.getBaseType(GetNameWithoutFolders(name)) == typeof(BaseNode))
            return true;
        return false;
    }
    // Check if given file name exists in BT_Files or its subfolders.
    // Returns path relative to BT_Files, null if file is not found.
    // Assumes xml list and subfolder lists are already populated.
    // TODO: See if this should return the path relative to Assets instead.
    // HACK: yes.
    string XMLExists(string file)
    {
        //string path = TreeData.xml_folderpath + "/" + file;
        //// check main folder first
        //foreach (string s in treeFiles)
        //{
        //    if (s == path)
        //        return file;
        //    // try adding subfolder names
        //    foreach (string r in tree_subfolders)
        //    {
        //        string temp = r + "/" + file;
        //        if (s == TreeData.xml_folderpath + "/" + temp)
        //            return temp;
        //    }
        //}
        //return null;

        // Check if file exists
        string xmlFolder = "Assets/" + TreeData.xml_folderpath;
        string[] folders = new string[] { xmlFolder };
        string[] files = AssetDatabase.FindAssets(filename, folders);
        if (files.Length > 0)
        {
            return null;
        }
        return null;
    }
    /// <summary>
    /// Node creation/destruction functions.
    /// </summary>    
    
    // Adds new node with chosen type.
    void CreateNode(string name, NODETYPES type)
    {
        nodes.Add(new node(type,
                           GetNameWithoutFolders(name), 
						   GetUniqueId(), 
						   GetUniqueId(), 
						   scrollPosition.x + nodePosX,
                           scrollPosition.y + controlArea_height, 
						   nodeWidth, 
						   nodeHeight));
    }
    void CreateNode(string name, NODETYPES type, float posX, float posY)
    {
        nodes.Add(new node(type,
                           GetNameWithoutFolders(name), 
						   GetUniqueId(), 
						   GetUniqueId(), 
						   scrollPosition.x + posX, 
						   scrollPosition.y + posY, 
						   nodeWidth, 
						   nodeHeight));
    }

    void KillNode(node node)
    {
        node.isDead = true;
        // also kill children, if invisible:
        if (node.children.Count != 0)
        {
            foreach (node n in node.children)
            {
                if (!n.isVisible)
                    KillNode(n);                
            }
        }       
    }
    void KillConnections(node node)
    {
        foreach (node n in nodes)
        {
            if (n.children.Contains(node))
            {

                n.children.Remove(node);
                n.spaceNeeded = parentspace(n);
            }
        }
    }
    // TODO: Better system for defining unique ID for everything?
    int GetUniqueId()
    {
        int id = idi;
        idi++;
        return id;
    }
    // Returns index of said node. Returns -1 if node is not found.
    int FindNodeWithId(int id)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].uniqueID == id)
                return i;
        }
        return -1;
    }
    // Returns index of node with said note ID. This is necessary because of window drawing.
    int FindNodeWithNoteId(int noteId)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].noteID == noteId)
                return i;
        }
        return -1;
    }
    // Get node type by name. Folders included in name.
    NODETYPES GetType(string name)
    {
        if (name == "RootNode")
            return NODETYPES.ROOT;
        foreach(string s in internaltypes)
        {
            if (s == name)
                return NODETYPES.INTERNAL;
        }
        foreach (string s in actiontypes)
        {
            if (s == name)
                return NODETYPES.LEAF;
        }
        foreach (string s in conditiontypes)
        {
            if (s == name)
                return NODETYPES.LEAF;
        }
        foreach (string s in decoratortypes)
        {
            if (s == name)
                return NODETYPES.DECORATOR;
        }
        return NODETYPES.INVALID;  /////// Node name does not exist. Can still be drawn! Must be marked as invalid, though.
    }

    /// <summary>
    /// BT structure functions
    /// </summary>

    // Keep track of tree depth. TODO: set warning value, which can be adjusted
    int GetUpdatedTreeDepth(node current, int curLevel)
    {
        if (current.treeLevel < curLevel)
            current.treeLevel = curLevel;
        if (current.children.Count == 0)
            return curLevel;
        else
        {
            int nextlevel = curLevel + 1;
            foreach (node n in current.children)
            {

                int temp = GetUpdatedTreeDepth(n, curLevel + 1);
                if (nextlevel < temp)
                    nextlevel = temp;
            }
            return nextlevel;
        }
    }
    // Show/Hide children toggle
    void SetVisibilityOfChildren(node root, bool value)
    {
        foreach (node n in root.children)        
        {
            n.isVisible = value;
            SetVisibilityOfChildren(n, value);
        }
    }
    void ConnectNodes(node parent, node child)
    {
        if (parent.children.Contains(child))
        {
            parent.children.Remove(child);            
        }
        else if (IsLegalConnection(child, parent))
        {
            parent.children.Add(child);
            parent.spaceNeeded = parentspace(parent);            
        }
        foreach (node n in nodes)
            n.treeLevel = 0;
        updatePriority(parent);
        treeDepth = GetUpdatedTreeDepth(FindRoot(), 0);
    }
    bool IsLegalConnection(node child, node proposed_parent)
    {
		// Can't connect to self!
        if (child == proposed_parent)
            return false;
        // Root and decorator are allowed only one child
        if ((proposed_parent.nodetype == NODETYPES.ROOT || 
			 proposed_parent.nodetype == NODETYPES.DECORATOR) 
			 && proposed_parent.children.Count > 0)
            return false;
        // To avoid looping descendant can't be parent
        if (CheckChildren(child, proposed_parent))
            return false;
        return true;
    }
    // Returns true if node with given ID exists somewhere in parent's descendants.
    bool CheckChildren(node parent, node IdToFind)
    {
        if (parent.children.Count > 0)
        {
            if (parent.children.Contains(IdToFind))
                return true;
            else
            {
                for (int i = 0; i < parent.children.Count; i++ )
                {                    
                    node next = parent.children[i];
                    if (CheckChildren(next, IdToFind))
                        return true;
                }
            }
        }
        return false;
    }

    // Define priority order of children by y coordinate: order children around!
    void updatePriority(node parent)
    {
        for(int j = 0; j < parent.children.Count -1; j++)
        {
            for (int i = 0; i < parent.children.Count - 1; i++)
            {
                if (parent.children[i].windowRect.y > parent.children[i+1].windowRect.y)
                {
                    node temp = parent.children[i];
                    parent.children[i] = parent.children[i + 1];
                    parent.children[i + 1] = temp;
                }
            }
        }
    }
    // Check all nodes in editor, detect childless internals
    bool EndsInLeaf()
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].children.Count == 0)
            {
                if (nodes[i].nodetype != NODETYPES.LEAF)
                {                    
                    return false;
                }
            }
        }
        return true;
    }
    // Check that all nodes in editor are connected to root.
    // NB! This currently happens every frame and DOES NOT WORK with multiparenting!
    // TODO: Change to use bool var / check each node ID in root descendants (multiparenting).
    bool ConnectsToRoot()
    {
        int counter = 1;
        childCount(FindRoot(), ref counter);
        if (nodes.Count > counter)
            return false;
        return true;
    }
    void childCount(node parent, ref int sofar)
    {
        
        sofar += parent.children.Count;
        for (int i = 0; i < parent.children.Count; i++)
        {
            if (parent.children[i].children.Count != 0)
                childCount(parent.children[i], ref sofar);
        }
    }
    //Returns root node, null if no root is found. TODO: Error check! All trees must have root.
    node FindRoot()
    {        
        for (int i = 0; i < nodes.Count; i++ )
        {
            if (nodes[i].nodetype == NODETYPES.ROOT)
                return nodes[i];
        }
        return null;
    }

    /// <summary>
    /// Automatic layout functions.
    /// </summary>
    
    void HierarchyLayout(node root)
    {
        float curY = 0;
        for (int i = 0; i < root.children.Count; i++)
        {
            root.children[i].windowRect.x = root.children[i].treeLevel * nodeOffsetX;
            // TODO: Find out what the hell this actually does. Works, though.
            float height = root.windowRect.y - root.spaceNeeded / 2 + (root.children[i].spaceNeeded) / 2 + curY;
            curY += root.children[i].spaceNeeded;
            root.children[i].windowRect.y = height;

            if (root.children[i].children.Count != 0)
                HierarchyLayout(root.children[i]);
            // Adjust position so that notes don't hide other nodes. 
            if (root.children[i].showNotes)
                root.children[i].windowRect.y -= 25;
        }        
    }

    float parentspace(node parent)
    {
        float ownOffset = 0;
        foreach (node n in parent.children)
        {
            if (n.isVisible)
                ownOffset += parentspace(n);
            else ownOffset = 0;
        }
        if (ownOffset == 0)
        { 
            ownOffset = nodeOffsetY;
            if (parent.showNotes)
                ownOffset += 50;
        }
        parent.spaceNeeded = ownOffset;
        return ownOffset;
    }
    void AdjustRootPosition()
    {
        node root = FindRoot();
        root.windowRect.x = 0;
        root.windowRect.y = root.spaceNeeded / 2 + controlArea_height;
    }
}
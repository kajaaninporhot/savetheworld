﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* This is the behavior component to be added to gameobjects. Trees are references to
 * this object's tree components and updated whenever this component is active, 
 * according to their update settings. 
 * TODO: For easy usage variable access should be as simple as possible 
 * (e.g. a Get method with only the variable name as parameter). Since this version
 * assumes behaviors as components to agent (as per Unity's basic logic), node specific
 * data can be stored in the node itself. Only agent specific (and tree specific, 
 * except for basic tree structure functionality) data needs to be stored on 
 * blackboard-like structure. Should this be a separate component or part of the 
 * behavior manager?
 * 
 * Currently there are two working versions of blackboard. The first is a general object type
 * dictionary with get and set methods on the behave component itself, the more advanced 
 * version is in a separate BT_Blackboard class and has typed dictionaries for common data types
 * and also an object type dictionary for custom types.
 * 
 * Typed dictionaries cause less gargabe, which may be useful for mobile applications.
 * 
 * If we need tree memories too, they can be stored in array corresponding the tree array. 
 * Should they be accessed from outside?
 * */

public class BT_Behave : MonoBehaviour {

    public BTree[] trees;
    // Blackboard-class contains unboxed versions of most common data types. Less garbage at runtime but first call
    // still causes garbage spike.
	// NB! variables from BT_Blackboard are not (currently) shown in Inspector.
    public BT_Blackboard blackBoard = new BT_Blackboard();
    // TODO: Send memory contents to inspector. Memory should not be public!
    public Dictionary<string, object> memory = new Dictionary<string, object>();
    // Causes garbage. Boxing? Boxing!
    public void Set<T>(T value, string variableName)
    {
        if (!KeyExists(variableName))
        {
            memory.Add(variableName, value);
        }
        else memory[variableName] = value;
    }
    public T Get<T>(string variableName)
    {
        try
        {
            return (T)memory[variableName];
        }
        catch (System.Collections.Generic.KeyNotFoundException)
        {
            Debug.LogError("Variable " + variableName + " not found in blackboard");

        }
        return default(T);
    }
    public bool KeyExists(string variableName)
    {
        var enumer = memory.GetEnumerator();
        while (enumer.MoveNext())
        {
            var element = enumer.Current;
            if (element.Key == variableName)
                return true;
        }
        return false;
    }


    void Awake()
    {
        trees = this.gameObject.GetComponents<BTree>();
        
    }
	void Start () 
    {
        foreach (BTree t in trees)
        {
            if (t.enabled)
            {
                t.InitialiseTreeVariables(this);
            }
        }
	}
    public bool show;
    
    // TODO: Better handling of individual tree execution. Not every tree should be checked on every frame.

    // Note: User should have full control of when to call different trees. Reactionary logic should be in Update;
    // event-specific decision logic should be accessed independently for each tree; non-critical logic run at 
    // intervals or divided over several frames...
    void Update() 
    {
        foreach (BTree t in trees)
        {
            if (!t.erroredOut)
                t.BT_Update();
        }
	}
    public BT_State RunTree(string treename)
    {
        for (int i = 0; i < trees.Length; i++)
        {
            if (trees[i].file.name == treename) // slow.
                return trees[i].BT_Update();
        }
        Debug.LogError("Behavior tree " + treename + " not found");
        return BT_State.ERROR;
    }
}


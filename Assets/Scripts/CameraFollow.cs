﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public float CameraWidth = 8;
    public Transform target;
    
	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (target.position.x >= CameraWidth)
            transform.position = new Vector3(target.position.x, transform.position.y, transform.position.z);
        else
            transform.position = new Vector3(CameraWidth, transform.position.y, transform.position.z);
	}
}

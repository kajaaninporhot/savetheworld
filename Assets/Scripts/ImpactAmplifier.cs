﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class ImpactAmplifier : MonoBehaviour
{
    Rigidbody2D body;
    public float offset;
    public float multiplier;
    [Space(6)]
    public LayerMask target_layers;

	// Use this for initialization
	void Start ()
    {
        body = GetComponent<Rigidbody2D>();
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (onLayerMask(col.collider.gameObject.layer))
        {
            Vector2 dir = (Vector2)this.transform.position - col.contacts[0].point;
            dir.Normalize();
            this.body.AddForce(dir * multiplier, ForceMode2D.Impulse);
        }
    }

    private bool onLayerMask(int layer)
    {
        return (target_layers.value & (int)Mathf.Pow(2, layer)) > 0;
    }
}

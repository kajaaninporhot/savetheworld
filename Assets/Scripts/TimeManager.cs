﻿using UnityEngine;
using System.Collections.Generic;

public class TimeManager : MonoBehaviour
{
    List<DangerTimeSlow> bullet_times;
    public float slowest_time;

	// Use this for initialization
	void Start ()
    {
        this.slowest_time = 1.0f;
        Debug.Log("Time manager initializing");
        bullet_times = new List<DangerTimeSlow>();
        foreach (DangerTimeSlow slow in FindObjectsOfType<DangerTimeSlow>())
            bullet_times.Add(slow);
        Debug.Log("Time manager initialization completed, " + bullet_times.Count + " time controllers found.");
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        this.slowest_time = 1.0f;
        foreach (DangerTimeSlow slow in bullet_times)
        {
            if (slow.current_slow_down < slowest_time)
                this.slowest_time = slow.current_slow_down;
        }
        Time.timeScale = this.slowest_time;
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class InputHandler : MonoBehaviour
{
    PlayerController controller;
    [Space(6)]
    public int player;

    bool use_mouse;

    float trigger_right;
    float trigger_left;
    float tr_current;
    float tl_current;
    [Space(6)]
    public float trigger_speed;

    float horizontal_left_axis;
    float vertical_left_axis;
    float hl_current;
    float vl_current;
    public float left_stick_speed;

    float horizontal_right_axis;
    float vertical_right_axis;
    float hr_current;
    float vr_current;
    public float right_stick_speed;

	// Use this for initialization
	void Start ()
    {
        controller = GetComponent<PlayerController>();
	}

    // Update is called once per frame
    void Update()
    {
        use_mouse = Input.GetJoystickNames().Length <= 1;


        //Fetch the most recent velocity
        controller.fetchVelocity();

        switch (player)
        {
            case 1:
            {
                //Fetch most recent axis states
                trigger_left = Input.GetAxis("TriggerLeft_1");
                trigger_right = Input.GetAxis("TriggerRight_1");
               
                horizontal_left_axis = Input.GetAxis("HL_1");
                vertical_left_axis = Input.GetAxis("VL_1");

                horizontal_right_axis = Input.GetAxis("HR_1");
                vertical_right_axis = Input.GetAxis("VR_1");

                if (Input.GetButtonDown("X_1"))
                    controller.kickPress();

                //Legs
                if (vertical_left_axis <= 0)
                    controller.Legs.control(-vertical_left_axis);

                //Jump
                if (vertical_left_axis > 0.5)
                    controller.jumpPress(horizontal_left_axis);

                //Smoothen received input
                moveTriggers(trigger_speed);
                moveRightStick(right_stick_speed);
                moveLeftStick(left_stick_speed);

                //Control arms and velocity
                controller.Torso.control(hl_current, vl_current, tl_current, tr_current);
                controller.controlVelocity(hl_current * (1.0f - tr_current * 0.5f), vl_current);
                controller.LeftArm.control(0, 0, tl_current, tr_current);
                controller.RightArm.control(hr_current, vr_current, tr_current, true);
                break;
            }
            case 2:
            {
                //Fetch most recent axis states
                trigger_left = Input.GetAxis("TriggerLeft_2");
                trigger_right = Input.GetAxis("TriggerRight_2");

                horizontal_left_axis = Input.GetAxis("HL_2");
                vertical_left_axis = Input.GetAxis("VL_2");

                if (!use_mouse)
                {
                    horizontal_right_axis = Input.GetAxis("HR_2");
                    vertical_right_axis = Input.GetAxis("VR_2");
                }
                else
                {
                    horizontal_right_axis = Input.GetAxis("Mouse X");
                    vertical_right_axis = Input.GetAxis("Mouse Y");

                    if (horizontal_right_axis > 1)
                        horizontal_right_axis = 1;
                    if (vertical_right_axis > 1)
                        vertical_right_axis = 1;

                    if (horizontal_left_axis == float.NaN)
                        horizontal_left_axis = 0;
                    if (vertical_right_axis == float.NaN)
                        vertical_right_axis = 0;
                }

                if (vertical_left_axis <= 0)
                    controller.Legs.control(-vertical_left_axis);

                //Jump
                if (vertical_left_axis > 0.5)
                    controller.jumpPress(horizontal_left_axis);

                //Smoothen received input
                moveTriggers(trigger_speed);
                moveRightStick(right_stick_speed);
                moveLeftStick(left_stick_speed);

                //Control arms and velocity
                controller.Torso.control(horizontal_left_axis, vertical_left_axis, trigger_left, trigger_right);
                controller.controlVelocity(hl_current * (1.0f - tr_current * 0.5f), vl_current);
                controller.LeftArm.control(0, 0, tl_current, tr_current);
                controller.RightArm.control(hr_current, vr_current, tr_current, false);
                break;
           }
        }
    }

    //speed should be between 0 and 1
    private void moveTriggers(float speed)
    {
        smoothTransition(ref this.tl_current, trigger_left, speed);
        smoothTransition(ref this.tr_current, trigger_right, speed);
    }
    //speed should be between 0 and 1
    private void moveRightStick(float speed)
    {
        smoothTransition(ref this.hr_current, horizontal_right_axis, speed);
        smoothTransition(ref this.vr_current, vertical_right_axis, speed);
    }
    //speed should be between 0 and 1
    private void moveLeftStick(float speed)
    {
        smoothTransition(ref this.hl_current, horizontal_left_axis, speed);
        smoothTransition(ref this.vl_current, vertical_left_axis, speed);
    }

    private void smoothTransition(ref float origin, float target, float speed)
    {
        origin = origin * speed + target * (1.0f - speed);
    }

}

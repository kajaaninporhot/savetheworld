﻿using UnityEngine;
using System.Collections;

public class TorsoController : MonoBehaviour
{
    public Animator TorsoAnimator;

    public void control(float h, float v, float left_trigger, float right_trigger)
    {
        if (right_trigger > 0.2)
            TorsoAnimator.SetFloat("bend_state", h);
        else
            TorsoAnimator.SetFloat("bend_state", 0.0f);
    }
}

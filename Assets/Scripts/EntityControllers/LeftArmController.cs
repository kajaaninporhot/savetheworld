﻿using UnityEngine;

public class LeftArmController : MonoBehaviour
{
    public Animator LeftArmAnimator;
    public Transform shoulder;

    public void control(float h, float v, float left_trigger, float right_trigger)
    {
        //Animation controls where the arm is at specific trigger states
        LeftArmAnimator.SetFloat("shield_state", left_trigger);
        //LeftArmAnimator.SetFloat("shove_state", left_trigger);
    }
}

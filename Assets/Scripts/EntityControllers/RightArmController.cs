﻿using UnityEngine;
using System.Collections;

public class RightArmController : MonoBehaviour
{
    public float Strength;

    public Transform knife_target;
    public Transform hinge_body;
    public Rigidbody2D moveable;

    //Control right arm
    public void control(float h, float v, float trigger, bool facing_right)
    {
        //Point knife towards ze enemy
        moveable.MoveRotation(0);// trigger_right * -75.0f);// knife_target.rotation.eulerAngles.z);

        Vector2 direction = new Vector2(h, v) * 1.8f;
        Vector2 dif = moveable.transform.position - hinge_body.position;

        if (direction.magnitude < 0.2)
            handleResting(dif, facing_right);
        else
            handleActiveMove(direction, dif, facing_right);
    }

    private void handleResting(Vector2 dif, bool facing_right)
    {
        Vector2 direction;

        if (facing_right)
        {
            direction = new Vector2(-0.7f, -0.5f) * 1.8f;
        }
        else
        {
            //hand.MoveRotation(trigger_right * 75.0f);// knife_target.rotation.eulerAngles.z);
            direction = new Vector2(0.7f, -0.5f) * 1.8f;
        }

        direction -= dif;
        moveable.AddForce(direction * Strength);
    }

    private void handleActiveMove(Vector2 dir, Vector2 dif, bool facing_right)
    {
        dir -= dif;
        moveable.AddForce(dir * Strength);
    }
}

﻿using UnityEngine;
using System.Collections;

public class LegController : MonoBehaviour
{
    public Animator LegAnimator;

    public void updateAnimation(float h_speed, float v_speed, bool onGround)
    {
        LegAnimator.SetFloat("speed_h", h_speed);
        LegAnimator.SetFloat("speed_v", v_speed);
        LegAnimator.SetBool("on_ground", onGround);
    }
    public void kick()
    {
        LegAnimator.SetTrigger("kick");
    }
    public void control(float h_axis)
    {
        LegAnimator.SetFloat("crouch_state", h_axis);
    }
}

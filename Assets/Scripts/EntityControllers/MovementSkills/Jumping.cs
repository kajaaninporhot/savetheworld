﻿using UnityEngine;
using System.Collections;
using System;

public class Jumping : MovementSkill
{
    public override void initialize()
    {
        base.initialize();
        this.style_name = "jumping";
    }

    public override string returnsTo()
    {
        return "walking";
    }
    public override bool doesThisBlock(MovementSkill skill)
    {
        switch (skill.name)
        {
            case "walking":
                return false;
            case "dash":
                return true;
            default:
                return true;
        }
    }

    protected override void applyImpulse()
    {
        Vector2 current_speed = body.getCurrentSpeed();
        current_speed.y = initial_impulse.y;
        body.setCurrentSpeed(current_speed);
    }
    public override void handleMoving(float horizontal_axis, float vertical_axis)
    {
        Vector2 current_speed = body.getCurrentSpeed();
        changeSpeedTowards(ref current_speed.x, speed.x * horizontal_axis, acceleration.x);
        changeSpeedTowards(ref current_speed.y, speed.y * vertical_axis, acceleration.y);
        body.setCurrentSpeed(current_speed);
    }
}

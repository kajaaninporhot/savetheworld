﻿using UnityEngine;
using System.Collections;

public class Stagger : MonoBehaviour
{
    public bool triggered { private set; get; }

    float dir_sign;
    public Vector2 speed;
    public Vector2 acceleration;

    public void start(float dir)
    {
        this.dir_sign = dir;
        this.triggered = true;
    }
    public void stop()
    {
        this.triggered = false;
    }

    public void handleMoving(float horizontal_axis, float vertical_axis, ref Vector2 current_speed)
    {
        changeSpeedTowards(ref current_speed.x, speed.x * dir_sign, acceleration.x);
        changeSpeedTowards(ref current_speed.y, speed.y, acceleration.y);
    }
    public virtual void changeSpeedTowards(ref float n, float target, float speed)
    {
        if (n == target)
            return;

        float dir = Mathf.Sign(target - n);
        n += speed * Time.deltaTime * dir;

        if (dir != Mathf.Sign(target - n))
            n = target;
    }
}

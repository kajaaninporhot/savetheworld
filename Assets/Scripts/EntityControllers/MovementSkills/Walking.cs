﻿using UnityEngine;
using System.Collections;

public class Walking : MovementSkill
{
    public override void handleMoving(float horizontal_axis, float vertical_axis)
    {
        Vector2 current_speed = body.getCurrentSpeed();
        changeSpeedTowards(ref current_speed.x, speed.x * horizontal_axis, acceleration.x);
        changeSpeedTowards(ref current_speed.y, speed.y * vertical_axis, acceleration.y);
        body.setCurrentSpeed(current_speed);
    }
    public override void initialize()
    {
        base.initialize();
        this.style_name = "walking";
    }

    public override string returnsTo()
    {
        return "idle";
    }
    public override bool doesThisBlock(MovementSkill skill)
    {
        switch (skill.style_name)
        {
            case "dash":
                return false;
            case "crouch":
                return false;
            case "jumping":
                return false;
            default:
                return true;
        }
    }
}

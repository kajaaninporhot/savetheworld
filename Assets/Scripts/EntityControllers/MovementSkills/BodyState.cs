﻿using UnityEngine;
using System.Collections;

public abstract class BodyState : MonoBehaviour
{
    public string state_name { protected set; get; }

    void Start()
    {
        initialize();
    }

    public abstract void initialize();
    public abstract string returnsTo();
    public abstract bool doesThisBlock(MovementSkill skill);
    public abstract bool canStartFrom(BodyState state, MovementSkill skill);
}

﻿using UnityEngine;
using System.Collections;

public abstract class MovementSkill : MonoBehaviour
{
    protected MovingEntity body;
    public string style_name { protected set; get; }

    protected float dir;
    public Vector2 speed;
    public Vector2 ending_speed;
    public bool isLinear;
    public Vector2 acceleration;

    public Vector2 initial_impulse;

    public float cooldown;
    private float cooldown_left;

	// Use this for initialization
	void Start()
    {
        initialize();
	}
    public virtual void initialize()
    {
        body = GetComponent<MovingEntity>();
    }

    public abstract bool doesThisBlock(MovementSkill style);
    public bool canBeActivated()
    {
        return (this.cooldown_left > 0);
    }

    public abstract string returnsTo();

    public virtual void startSkill(float dir)
    {
        this.dir = dir;
        this.applyImpulse();
    }
    public virtual void stopSkill()
    {
        this.cooldown_left = this.cooldown;
    }

    protected virtual void applyImpulse()
    {
        Vector2 current_speed = body.getCurrentSpeed();
        current_speed = initial_impulse;
        body.setCurrentSpeed(current_speed);
    }
    public virtual void handleMoving(float horizontal_axis, float vertical_axis)
    {
        Vector2 current_speed = body.getCurrentSpeed();
        changeSpeedTowards(ref current_speed.x, speed.x, acceleration.x);
        changeSpeedTowards(ref current_speed.y, speed.y, acceleration.y);
        body.setCurrentSpeed(current_speed);
    }

    void Update()
    {
        update();
    }
    protected virtual void update()
    {
        if (this.cooldown_left > 0)
            this.cooldown_left -= Time.deltaTime;
    }

    public virtual void changeSpeedTowards(ref float n, float target, float speed)
    {
        if (n == target)
            return;

        float dir = Mathf.Sign(target - n);
        n += speed * Time.deltaTime * dir;

        if (dir != Mathf.Sign(target - n))
            n = target;
    }
}

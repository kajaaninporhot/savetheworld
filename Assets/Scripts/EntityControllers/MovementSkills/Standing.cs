﻿using UnityEngine;
using System.Collections;
using System;

public class Standing : BodyState
{
    public override void initialize()
    {
        this.state_name = "standing";
    }

    public override string returnsTo()
    {
        return "standing";
    }
    public override bool doesThisBlock(MovementSkill skill)
    {
        return false;
    }
    public override bool canStartFrom(BodyState state, MovementSkill skill)
    {
        switch (state.state_name)
        {
            case "crouching":
                switch (skill.style_name)
                {
                    case "walking":
                        return true;
                }
                return false;
            case "standing":
                return false;
            default:
                return false;
        }

    }
}

﻿using UnityEngine;
using System.Collections;

public class Dash : MovementSkill
{
    public LayerMask passed_layer;

    public override void initialize()
    {
        base.initialize();
        this.style_name = "dash";
    }

    public override void startSkill(float dir)
    {
        base.startSkill(dir);
        //Physics2D.IgnoreLayerCollision((int)Mathf.Log(entity_layer.value, 2), (int)Mathf.Log(entity_layer.value, 2), true);
    }
    public override void stopSkill()
    {
        base.stopSkill();
        body.setCurrentSpeed(new Vector2(ending_speed.x * dir, ending_speed.y));
        //Physics2D.IgnoreLayerCollision((int)Mathf.Log(entity_layer.value, 2), (int)Mathf.Log(entity_layer.value, 2), false);
    }

    public override string returnsTo()
    {
        return "idle";
    }
    public override bool doesThisBlock(MovementSkill skill)
    {
        switch (skill.style_name)
        {
            case "jump":
                return true;
            case "crouch":
                return true;
            default:
                return true;
        }
    }

    public override void handleMoving(float horizontal_axis, float vertical_axis)
    {
        body.setCurrentSpeed(new Vector2(speed.x * dir, speed.y));
    }
}

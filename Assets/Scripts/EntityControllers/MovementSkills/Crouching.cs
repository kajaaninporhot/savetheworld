﻿using UnityEngine;
using System.Collections;
using System;

public class Crouching : BodyState
{
    public override void initialize()
    {
        this.state_name = "crouching";
    }

    public override string returnsTo()
    {
        return "standing";
    }
    public override bool doesThisBlock(MovementSkill skill)
    {
        return true;
    }
    public override bool canStartFrom(BodyState state, MovementSkill skill)
    {
        switch (state.state_name)
        {
            case "standing":
                switch (skill.style_name)
                {
                    case "walking":
                        return true;
                    case "jumping":
                        return true;
                }
                return false;
            case "crouching":
                return false;
            default:
                return false;
        }

    }
}

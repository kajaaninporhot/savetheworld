﻿using UnityEngine;
using System.Collections;

public class GroundChecker : MonoBehaviour
{
    bool onGround;
    bool nearGround;

    public LayerMask ground_layer;
    public float ground_radius;
    public float near_ground_radius;

    public bool isOnGround(bool refresh)
    {
        if (refresh)
            onGround = Physics2D.OverlapCircle(transform.position, ground_radius, ground_layer);
        return onGround;
    }
    public bool isNearGround(bool refresh)
    {
        if (refresh)
            nearGround = Physics2D.OverlapCircle(transform.position, near_ground_radius, ground_layer);
        return nearGround;
    }
}

﻿using UnityEngine;
using System.Collections;

public class BaseEntity : MonoBehaviour
{
    protected bool facingRight = true;
    protected GroundChecker ground_checker;

    [Header("-- Base Entity Settings --")]
    public float MaxHealth;
    public float CurrentHealth;
    [Space(6)]

    public bool onGround;
    public bool nearGround;
    [Space(6)]

    public Rigidbody2D CenterOfWeight;

    // Use this for initialization
    protected void Start()
    {
        this.ground_checker = GetComponentInChildren<GroundChecker>();
    }

    protected void Update()
    {
        checkOnGround();
        checkNearGround();
    }

    public float getRelationalHealth()
    {
        return this.CurrentHealth / this.MaxHealth;
    }

    public void dealDamage(float amount)
    {
        this.CurrentHealth -= amount;
        if (this.CurrentHealth <= 0)
            killMe();
    }

    private void killMe()
    {
        //Disable player controls
        this.enabled = false;

        //Disable mimics
        MimicMovement[] mimics = GetComponentsInChildren<MimicMovement>();
        foreach (MimicMovement mimic in mimics)
            mimic.enabled = false;    

        Destroy(transform.gameObject, 5);
    }

    protected void checkOnGround()
    {
        onGround = ground_checker.isOnGround(true);
    }
    protected void checkNearGround()
    {
        nearGround = ground_checker.isNearGround(true);
    }
}

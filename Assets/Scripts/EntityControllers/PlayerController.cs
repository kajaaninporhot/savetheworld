﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Walking))]
public class PlayerController : MovingEntity
{
    //Movement related states
    private BodyState current_body_state;
    private MovementSkill current_movement_skill;

    //Movement Skills
    private Walking walking;
    private Jumping jumping;

    //Body states
    private Standing standing;

    [Header("-- Player Controller Settings --")]
    public LeftArmController LeftArm;
    public RightArmController RightArm;
    public TorsoController Torso;

    [Space(6)]
    public List<BleedController> bleeding_controllers;

    // Use this for initialization
    new protected void Start()
    {
        base.Start();

        standing = GetComponent<Standing>();

        walking = GetComponent<Walking>();
        jumping = GetComponent<Jumping>();

        current_body_state = standing;
        current_movement_skill = walking;

        //Get all bleeding controllers
        bleeding_controllers = new List<BleedController>();
        foreach (BleedController bc in GetComponentsInChildren<BleedController>())
            bleeding_controllers.Add(bc);
    }

    new void Update()
    {
        base.Update();
    }
    new void LateUpdate()
    {
        base.LateUpdate();

        //Apply damage
        foreach (BleedController bc in bleeding_controllers)
        {
            this.dealDamage(bc.getDamage());
        }

        //Handle walking
        if (current_movement_skill == jumping && onGround && current_speed.y < 5)
        {
            switch (jumping.returnsTo())
            {
                case "walking":
                    current_movement_skill = walking;
                    break;
            }
        }

        applyFrictions();
        CenterOfWeight.velocity = new Vector2(current_speed.x, current_speed.y);
    }

    public void fetchVelocity()
    {
        this.current_speed = CenterOfWeight.velocity;
    }
    public void controlVelocity(float horizontal_axis, float vertical_axis)
    {
        current_movement_skill.handleMoving(horizontal_axis, vertical_axis);
        /*
        if (crouching.triggered)
        {
            crouching.handleMoving(horizontal_axis, vertical_axis);
        }
        else if (dash.triggered)
        {
            dash.handleMoving(horizontal_axis, vertical_axis);
        }
        else
        {
            if (onGround)
                walking.handleMoving(horizontal_axis, vertical_axis, ref current_speed);
            else
                jumping.handleMoving(horizontal_axis, vertical_axis, ref current_speed);
        }       
        */
    }

    public void jumpPress(float horizontal_axis)
    {
        if (!current_body_state.doesThisBlock(jumping) && !current_movement_skill.doesThisBlock(jumping))
        {
            if (onGround)
            {
                //Debug.Log("We're jumping");
                jumping.startSkill(Mathf.Sign(horizontal_axis));
                current_movement_skill = jumping;
                jumpOrdered = false;
            }
            else if(nearGround)
            {
                jumpOrdered = true;
            }
        }
    }
    public void kickPress()
    {
        Legs.kick();
    }

    //Animation callback
    public void stopMovementSkill()
    {
        current_movement_skill.stopSkill();
        switch (current_movement_skill.returnsTo())
        {
            case "walking":
                current_movement_skill = walking;
                break;
        }
    }
}

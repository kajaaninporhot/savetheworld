﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class AI_Input : MonoBehaviour
{
    public enum TopSwingState
    {
        Waiting,
        Right,
        Top,
        Left
    }
    public enum LowSwingState
    {
        Waiting,
        Right,
        Bottom,
        Left
    }

    BT_Behave blackboard;

    float enemy_dir;

    public Transform me;
    public Transform opponent;
    public Transform opponent_knife;

    PlayerController controller;

    private Vector2 hand_back;
    private Vector2 hand_up;
    private Vector2 hand_left;
    private Vector2 hand_right;

    float stage_cooldown = 0.3f;
    float cur_stage_cooldown;
    TopSwingState top_state = TopSwingState.Waiting;
    LowSwingState low_state = LowSwingState.Waiting;

    // Use this for initialization
    void Start ()
    {
        blackboard = GetComponent<BT_Behave>();
        controller = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        setFlags();

        enemy_dir = opponent.position.x - me.position.x;
        
        //Approaching
        if (blackboard.Get<bool>("approach"))
        {
            if (enemy_dir < 0)
                controller.controlVelocity(-0.5f, 0);
            else
                controller.controlVelocity(0.5f, 0);
        }
        else
        {
            controller.controlVelocity(0, 0);
        }

        if (blackboard.Get<bool>("top_swinging") && top_state == TopSwingState.Waiting)
        {
            cur_stage_cooldown = stage_cooldown;
            top_state = TopSwingState.Right;
        }
        else if (blackboard.Get<bool>("low_swinging") && low_state == LowSwingState.Waiting)
        {
            cur_stage_cooldown = stage_cooldown;
            low_state = LowSwingState.Right;
        }

        //Update motion and advance swings
        if (top_state != TopSwingState.Waiting)
            topAttack();
        if (low_state != LowSwingState.Waiting)
            lowAttack();

	}

    private void topAttack()
    {
        //Cooldowns
        if (cur_stage_cooldown <= 0)
        {
            cur_stage_cooldown = stage_cooldown;

            if (top_state == TopSwingState.Left)
            {
                //Notify blackboard if we're done swinging
                top_state = TopSwingState.Waiting;
                blackboard.Set<bool>(false, "top_swinging");
            }
            else
                top_state += 1;
        }
        else
        {
            cur_stage_cooldown -= Time.deltaTime;
        }

        //Swing motion
        switch (top_state)
        {
            case TopSwingState.Right:
                controller.RightArm.control(1.0f, 0, 1.0f, false);
                break;
            case TopSwingState.Top:
                controller.RightArm.control(0.0f, 1.0f, 1.0f, false);
                break;
            case TopSwingState.Left:
                controller.RightArm.control(-1.0f, 0.0f, 1.0f, false);
                break;
        }
    }
    private void lowAttack()
    {
        //Cooldowns
        if (cur_stage_cooldown <= 0)
        {
            cur_stage_cooldown = stage_cooldown;

            if (low_state == LowSwingState.Left)
            {
                //Notify blackboard if we're done swinging
                low_state = LowSwingState.Waiting;
                blackboard.Set<bool>(false, "low_swinging");
            }
            else
                low_state += 1;
        }
        else
        {
            cur_stage_cooldown -= Time.deltaTime;
        }

        //Swing motion
        switch (low_state)
        {
            case LowSwingState.Right:
                controller.RightArm.control(1.0f, 0, 1.0f, false);
                break;
            case LowSwingState.Bottom:
                controller.RightArm.control(0.0f, -1.0f, 1.0f, false);
                break;
            case LowSwingState.Left:
                controller.RightArm.control(-1.0f, 0.0f, 1.0f, false);
                break;
        }
    }

    private void setFlags()
    {
        blackboard.Set<bool>(Mathf.Abs(opponent.position.x - me.position.x) < 3, "near");      
    }
}

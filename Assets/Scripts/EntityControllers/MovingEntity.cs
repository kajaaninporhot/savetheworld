﻿using UnityEngine;
using System.Collections;

public class MovingEntity : BaseEntity
{
    protected bool jumpOrdered;
    protected Vector2 current_speed;

    [Header("-- Moving Entity Settings --")]
    public LegController Legs;
    [Space(6)]
    public Vector2 GroundFriction = new Vector2(0.96f, 1.0f);
    public Vector2 AirFriction = new Vector2(1.0f, 1.0f);
    [Space(6)]
    public float ReactionPercentage = 1.2f;
    public float AirPercentage = 0.6f;


    // Use this for initialization
    new void Start()
    {
        base.Start();
	}

    public Vector2 getSpeed()
    {
        return this.current_speed;
    }
    public void setSpeed(Vector2 speed)
    {
        this.current_speed = speed;
    }

    public Vector2 getCurrentSpeed()
    {
        return this.current_speed;
    }
    public void setCurrentSpeed(Vector2 speed)
    {
        this.current_speed.x = speed.x;
        this.current_speed.y = speed.y;
    }

	// Update is called once per frame
	new void Update ()
    {
        base.Update();
        applyFrictions();

        //controlVelocity();
        Debug.Log(current_speed);
        CenterOfWeight.velocity = new Vector2(current_speed.x, current_speed.y);
    }
    protected void LateUpdate()
    {
        Legs.updateAnimation(Mathf.Abs(CenterOfWeight.velocity.x), CenterOfWeight.velocity.y, onGround);
    }

    protected void applyFrictions()
    {
        if (onGround)
            applyFriction(GroundFriction);
        else
            applyFriction(AirFriction);

    }  
    private void applyFriction(Vector2 friction)
    {
        this.current_speed.x *= friction.x;
        this.current_speed.y *= friction.y;
    }
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TrailRenderer))]
public class TrailTimeScale : MonoBehaviour
{
    TrailRenderer renderer;
    float base_time;

	// Use this for initialization
	void Start ()
    {
        renderer = GetComponent<TrailRenderer>();
        base_time = renderer.time;
	}
	
	// Update is called once per frame
	void Update ()
    {
        renderer.time = (1.0f - Time.timeScale) * base_time;
	}
}

﻿using UnityEngine;
using System.Collections;

public class IgnoreCollisionBetween : MonoBehaviour
{
    public LayerMask layer1;
    public LayerMask layer2;

    // Use this for initialization
    void Start ()
    {
        bool[] layer1_bits = separateBits(layer1.value);
        bool[] layer2_bits = separateBits(layer2.value);

        for (int a = 0; a < 32; a++)
        {
            for (int b = 0; b < 32; b++)
            {
                if (layer1_bits[a] && layer2_bits[b])
                    Physics2D.IgnoreLayerCollision(a, b, true);
            }
        }
    }

    public bool[] separateBits(int layer)
    {
        bool[] bits = new bool[32];
        int mask = 1;

        for (int a = 0; a < 32; a++)
        {
            bits[a] = (layer & mask) > 0;
            mask <<= 1;
        }

        return bits;
    }
}

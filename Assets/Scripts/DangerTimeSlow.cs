﻿using UnityEngine;
using System.Collections;

public class DangerTimeSlow : MonoBehaviour
{
    public Transform dangerous_entity;
    [Space(6)]
    public float starting_distance;
    public float ending_distance;
    public float final_slow_down;

    public float current_slow_down = 1.0f;

	// Use this for initialization
	void Start ()
    {
        GetComponents<DangerTimeSlow>();
	}

    // Update is called once per frame
    void Update()
    {
        if (dangerous_entity == null)
            return;

        float distance = Vector2.Distance(dangerous_entity.position, transform.position);

        //Calculate proper slowdown
        if (distance < starting_distance)
            current_slow_down = final_slow_down;
        else if (distance < ending_distance && distance > starting_distance)
            current_slow_down = ((1.0f - final_slow_down) * Mathf.Pow((distance - starting_distance) / (ending_distance - starting_distance), 2) + final_slow_down);
        else
            current_slow_down = 1.0f;
    }
}

﻿using UnityEngine;
using System.Collections;

public class ParallelBG : MonoBehaviour
{
    [Tooltip("The percentage of the maps length this bg is, 0.5 etc.")]
    public float RelationalLength;

    public Transform player;

	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = new Vector2(player.position.x * RelationalLength, transform.position.y);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthTracker : MonoBehaviour
{
    [SerializeField]
    private BaseEntity tracked_player;

    [SerializeField]
    private Image content;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        content.fillAmount = tracked_player.getRelationalHealth();
	}
}

﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class Timer
{
    private int id;

    private TimedBlock target;
    private float orig_time;
    public float time { get; private set; }
    public bool loops { get; private set; }
    private bool triggered;

    public Timer(int id, float time, bool loops, TimedBlock listener)
    {
        this.id = id;

        this.target = listener;
        this.orig_time = time;
        this.time = time;

        this.loops = loops;
        this.triggered = false;
    }

    public void update()
    {
        this.time -= Time.deltaTime;
        if (time <= 0 && triggered == false)
        {
            this.target.timeIsUp(id);

            //Reset timer
            if (loops)
                this.time = orig_time;
            else
                this.triggered = true;
        }
    }
}

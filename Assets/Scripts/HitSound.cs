﻿using UnityEngine;
using System.Collections;

public class HitSound : MonoBehaviour
{
    public GameObject SoundManager;
    private AudioSource[] source;

    public float cooldown = 1.0f;
    private float cur_cooldown;

    // Use this for initialization
    void Start ()
    {
        source = SoundManager.GetComponents<AudioSource>();
	}

    void OnCollisionEnter2D()
    {
        if (cur_cooldown <= 0)
        {
            source[0].Play();
            cur_cooldown = cooldown;
        }
    }

    void Update()
    {
        if (cur_cooldown > 0)
            cur_cooldown -= Time.deltaTime;
    }
}

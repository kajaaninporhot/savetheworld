﻿using UnityEngine;
using System.Collections.Generic;

public class BleedController : MonoBehaviour
{
    List<ParticleSystem> particle_systems;
    List<float> weights;

    public int bleeder_count;
    public float bleeder_damage;

	// Use this for initialization
	void Start ()
    {
        particle_systems = new List<ParticleSystem>();
        weights = new List<float>();

        bleeder_count = 0;
        bleeder_damage = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (particle_systems.Count != weights.Count)
            Debug.Log("Mismatching counts of particles systems and weights in {" + name + "}");

        bleeder_damage = 0;
        for (int a = particle_systems.Count - 1; a >= 0; a--)
        {
            if (particle_systems[a] == null)
            {
                particle_systems.RemoveAt(a);
                weights.RemoveAt(a);
            }
            else
            {
                if (particle_systems[a].emission.enabled)
                    bleeder_damage += weights[a];
            }
        }
        bleeder_count = particle_systems.Count;
	}

    public float getDamage()
    {
        return bleeder_damage;
    }

    public void addBleeder(GameObject bleeder, float weight)
    {
        ParticleSystem sys = bleeder.GetComponent<ParticleSystem>();
        if (sys == null)
        {
            Debug.Log("Cannot add a bleeder, {" + name + "} GameObject does not have a particle system.");
        }
        else
        {
            this.particle_systems.Add(sys);
            this.weights.Add(weight);
        }
    }
}

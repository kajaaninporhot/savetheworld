﻿using UnityEngine;
using System.Collections;

public class Sparkler : MonoBehaviour {
    public Transform sparkle;

	// Use this for initialization
	void Start () {
        var e = sparkle.GetComponent<ParticleSystem>().emission;
        e.enabled = false;
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.gameObject.layer == 13 || col.collider.gameObject.layer == 17)
        {
            var e = sparkle.GetComponent<ParticleSystem>().emission;
            e.enabled = true;
            StartCoroutine(stopSparkles());
        }
    }
   

    IEnumerator stopSparkles()
    {
        yield return new WaitForSeconds(.1f);
        var e = sparkle.GetComponent<ParticleSystem>().emission;
        e.enabled = false;
    }
}

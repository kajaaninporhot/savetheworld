﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
public class Bleeder : MonoBehaviour
{
    bool side;
    BleedController bleeding_controller;

    public GameObject blood_spawner;
    public float BloodMultiplier = 1;

	void Start ()
    {
        this.side = checkSide(gameObject.layer);

        this.bleeding_controller = GetComponent<BleedController>();
        if (bleeding_controller == null)
            Debug.Log("{" + name + "} can bleed but has no bleeding controller.");
	}

    private bool checkSide(int layer)
    {
        return layer > 13;
    }

    void OnCollisionEnter2D(Collision2D col)
    {


        //Fetch the colliding collider's game object
        GameObject weapon = col.collider.gameObject;

        //Do not count self stabs
        if (checkSide(weapon.layer) == this.side)
            return;

        //Bleed only when struck by weapon
        if (weapon.layer == 13 || weapon.layer == 17)
            makeBlood(weapon, col);
    }

    private void makeBlood(GameObject weapon, Collision2D col)
    {
        VelocityEstimator estimator = weapon.GetComponent<VelocityEstimator>();

        //Estimate the collision speed
        float col_speed = 1.0f;
        if (estimator != null)
        {
            col_speed = estimator.getMagnitude() / 20.0f;
            col_speed = Mathf.Pow(col_speed, 1.2f);
        }

        //Multiply the collision speed with blood multiplier
        float combined_multiplier = BloodMultiplier * col_speed;

        GameObject blood_spawner = Instantiate(this.blood_spawner);
        blood_spawner.transform.parent = transform;
        blood_spawner.transform.position = col.contacts[0].point;
        //get stab direction
        Vector2 dif = (Vector2)this.transform.position - col.contacts[0].point;
        float dir = Mathf.Atan2(dif.y, dif.x);// col.collider.transform.rotation.eulerAngles.z;
        blood_spawner.transform.rotation = Quaternion.Euler(dir - 180, 90, 0);

        //Start blood
        ParticleSystem ps = blood_spawner.GetComponent<ParticleSystem>();
        var e = ps.emission;
        e.enabled = true;

        ps.startSpeed *= combined_multiplier / 1f;
        ps.startSize = ps.startSize * (combined_multiplier / 4f) + 0.20f;

        //enable the object
        blood_spawner.SetActive(true);

        if (bleeding_controller != null)
            bleeding_controller.addBleeder(blood_spawner, BloodMultiplier);
    }
}

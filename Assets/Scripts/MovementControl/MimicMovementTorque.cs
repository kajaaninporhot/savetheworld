﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MimicMovementTorque : MonoBehaviour
{
    public Transform target;
    public float speed = 10;
    public float follow_angle_offset;
    public float cutoff;

    Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();

        if (target == null)
            target = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        rotateTowardsTarget(clipAngle(transform.rotation.eulerAngles.z), clipAngle(target.rotation.eulerAngles.z + follow_angle_offset));
        //Debug.Log(target.eulerAngles + " | " + target.rotation.eulerAngles + " | " + target.localRotation.eulerAngles);
    }
    private float clipAngle(float angle)
    {
        while (angle > 180)
            angle -= 360;
        while (angle < -180)
            angle += 360;

        return angle;
    }
    
    private void rotateTowardsTarget(float origin, float target)
    {
        float pos_a = getPositiveDistance(origin, target);
        float neg_a = getNegativeDistance(origin, target);
        Debug.Log(pos_a + " " + neg_a + " | " + origin + " " + target);
        //Debug.Log(origin + " -> " + lerp_angle + " -> " + target);

        if (pos_a < neg_a)
        {
            if (Mathf.Abs(Mathf.DeltaAngle(origin, target)) > cutoff)
                body.AddTorque(speed * Time.deltaTime);
            //else
            //{
            //    body.AddTorque(-speed * Time.deltaTime);
            //}
            //else
              //  body.angularVelocity = (lerp_angle - origin) / Time.deltaTime;
        }
        else
        {
            if (Mathf.Abs(Mathf.DeltaAngle(origin, target)) > cutoff)
                body.AddTorque(-speed * Time.deltaTime);
            //else
            //{
            //    body.AddTorque(speed * Time.deltaTime);
            //}
            //else
            //    body.angularVelocity = (lerp_angle - origin) / Time.deltaTime;
        }
    }

    private float getPositiveDistance(float a, float b)
    {
        if (b > a)
            return b - a;
        else
            return (180 - a) + (180 + b);
    }
    private float getNegativeDistance(float a, float b)
    {
        if (b < a)
            return a - b;
        else
            return (180 + a) + (180 - b);
    }

}

﻿using UnityEngine;
using System.Collections.Generic;

public class VelocityEstimator : MonoBehaviour
{
    public int buffer_size;
    private Vector2[] location_buffer;
    private float[] location_weights;

    public bool UseTrail;
    public GameObject TrailPrefab;
    private GameObject[] Trails;

    //[SerializeField]
    //Vector2 current_speed;

    private int buffer_position;

    // Use this for initialization
    void Start()
    {
        this.buffer_position = 0;

        this.location_buffer = new Vector2[buffer_size];
        this.location_weights = new float[buffer_size];
        for (int a = 0; a < buffer_size; a++)
        {
            this.location_buffer[a] = Vector2.zero;
            this.location_weights[a] = (a + 1) / (float)buffer_size;
        }

        if (UseTrail)
        {
            Trails = new GameObject[buffer_size];
            for (int a = 0; a < buffer_size; a++)
            {
                Trails[a] = Instantiate(TrailPrefab);
            }
        }
    }

	// Update is called once per frame
	void Update ()
    {
        this.location_buffer[buffer_position] = transform.position;

        if (UseTrail)
            Trails[buffer_position].transform.position = transform.position;

        buffer_position = (buffer_position + 1) % buffer_size;

        
        //current_speed = getSpeed();
	}

    public float getMagnitude()
    {
        //return 5.0f;

        float result = 0.0f;
        Vector2 current_pos = transform.position;
        for (int a = 0; a < buffer_size; a++)
        {
            result += (current_pos - location_buffer[(a + buffer_position) % buffer_size]).magnitude * location_weights[a];
        }

        return result;
    }
}

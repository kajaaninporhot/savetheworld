﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MimicMovement : MonoBehaviour
{
    public Transform target;
    public float speed = 10000;
    public float follow_angle_offset;

    public float scaling_factor_offset = 5;

    Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();

        if (target == null)
            target = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //float current_target = clipAngle(target.rotation.eulerAngles.z);
        //Debug.Log(transform.lossyScale.x);
        if (transform.rotation.eulerAngles.y < 90)
        {
            //These parts are not rotated
            //Debug.Log("Normal partion");
            rotateTowardsTarget(clipAngle(transform.rotation.eulerAngles.z), clipAngle(target.rotation.eulerAngles.z + follow_angle_offset));
        }
        else
        {
            //These parts are mirrored
            rotateMirroredTowardsTarget(clipAngle(transform.rotation.eulerAngles.z), clipAngle(target.rotation.eulerAngles.z + follow_angle_offset));
        }
    /*
        else
        {
            Debug.Log("Inverted partion");

            if (current_target > 0)
            {
                rotateTowardsTarget(clipAngle(transform.rotation.eulerAngles.z), current_target - 2 * (current_target - 90) + follow_angle_offset);
            }
            else
            {
                rotateTowardsTarget(clipAngle(transform.rotation.eulerAngles.z), current_target - 2 * (current_target + 90) + follow_angle_offset);
            }


        }
        */
        //Debug.Log(target.eulerAngles + " | " + target.rotation.eulerAngles + " | " + target.localRotation.eulerAngles);
    }
    private float clipAngle(float angle)
    {
        while (angle > 180)
            angle -= 360;
        while (angle < -180)
            angle += 360;

        return angle;
    }
    private void rotateTowardsTarget(float origin, float target)
    {
        float scaling_factor;

        float pos_a = getPositiveDistance(origin, target);
        float neg_a = getNegativeDistance(origin, target);
        //float lerp_angle = Mathf.LerpAngle(origin, target, 0.5f);

        //Debug.Log(origin + " -> " + lerp_angle + " -> " + target);

      
        if (pos_a < neg_a)
        {
            scaling_factor = (pos_a + scaling_factor_offset) / 360.0f;
            //if (pos_a > Mathf.Abs(body.angularVelocity * Time.deltaTime))
            body.angularVelocity = speed * scaling_factor;
            //else
                //body.angularVelocity = (neg_a) / Time.deltaTime;      
        }
        else
        {
            scaling_factor = (neg_a + scaling_factor_offset) / 360.0f;
            //if (neg_a > Mathf.Abs(body.angularVelocity * Time.deltaTime))
            body.angularVelocity = -speed * scaling_factor;
            //else
                //body.angularVelocity = (pos_a) / Time.deltaTime;
        }
    }
    private void rotateMirroredTowardsTarget(float origin, float target)
    {
        float scaling_factor;

        float pos_a = getPositiveDistance(origin, target);
        float neg_a = getNegativeDistance(origin, target);
        //float lerp_angle = Mathf.LerpAngle(origin, target, 0.5f);

        //Debug.Log(origin + " -> " + lerp_angle + " -> " + target);


        if (pos_a < neg_a)
        {
            scaling_factor = (pos_a + scaling_factor_offset) / 360.0f;
            //if (pos_a > Mathf.Abs(body.angularVelocity * Time.deltaTime))
            body.angularVelocity = -speed * scaling_factor;
            //else
            //body.angularVelocity = (neg_a) / Time.deltaTime;      
        }
        else
        {
            scaling_factor = (neg_a + scaling_factor_offset) / 360.0f;
            //if (neg_a > Mathf.Abs(body.angularVelocity * Time.deltaTime))
            body.angularVelocity = speed * scaling_factor;
            //else
            //body.angularVelocity = (pos_a) / Time.deltaTime;
        }
    }

    private float getPositiveDistance(float a, float b)
    {
        if (b > a)
            return b - a;
        else
            return (180 - a) + (180 + b);
    }
    private float getNegativeDistance(float a, float b)
    {
        if (b < a)
            return a - b;
        else
            return (180 + a) + (180 - b);
    }

}

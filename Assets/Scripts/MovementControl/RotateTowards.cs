﻿using UnityEngine;
using System.Collections;

public class RotateTowards : MonoBehaviour
{
    public Transform target;
    public bool flipped;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 dif = Vector2.zero;

        if (target != null)
            dif = target.position - transform.position;
        else if (flipped)
            dif = new Vector2(-10, 5);
        else
            dif = new Vector2(10, 5);

        float rotation = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;

        if (flipped)
            rotation += 180;

        if (rotation > 180)
            rotation -= 360;

        //if (flipped == false)
            transform.rotation = Quaternion.Euler(0, 0, rotation);
        //else
          //  transform.rotation = Quaternion.Euler(0, 0, 180 - rotation);
    }
}

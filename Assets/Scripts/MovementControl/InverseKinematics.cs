﻿using UnityEngine;
using System.Collections;

public class InverseKinematics : MonoBehaviour
{
    public Vector2 getArmAngles(Vector2 s, Vector2 e, float l1, float l2)
    {
        Vector2 p = e - s;

        float B = Vector2.Distance(s, e);
        float B2 = B * B;
        float q1 = Mathf.Atan2(p.y, p.x);
        float q2 = Mathf.Acos((l1 * l1 - l2 * l2 + B2) / (2 * l1 * B));

        float a1 = q1 + q2;
        float a2 = Mathf.Acos((l1 * l1 + l2 * l2 - B2) / (2 * l1 * l2));

        return new Vector2(a1, a2);

        /*
        float y_option_1 = Mathf.Atan2(Mathf.Sqrt(1 - Mathf.Pow((p.x * p.x + p.y * p.y - l1 * l1 - l2 * l2) / (2 * l1 * l2),
                                                                2)),
                                       (p.x * p.x + p.y * p.y - (l1 * l1) - (l2 * l2)) / (2 * l1 * l2));
        float y_option_2 = Mathf.Atan2(-Mathf.Sqrt(1 - Mathf.Pow((p.x * p.x + p.y * p.y - l1 * l1 - l2 * l2) / (2 * l1 * l2),
                                                                 2)),
                                       (p.x * p.x + p.y * p.y - (l1 * l1) - (l2 * l2)) / (2 * l1 * l2));
        if (!float.IsNaN(y_option_1))
        {
            float x_option = Mathf.Atan2(Mathf.Sqrt(1 - Mathf.Pow((p.x * (l1 + l2 * Mathf.Cos(y_option_1) + p.y * l2 * Mathf.Sin(y_option_1))) / (p.x * p.x + p.y * p.y), 2)),
                               (p.x * (l1 + l2 * Mathf.Cos(y_option_1) + p.y * l2 * Mathf.Sin(y_option_1))) / (p.x * p.x + p.y * p.y));
            if (!float.IsNaN(x_option))
                return new Vector2(y_option_1, x_option);

            x_option = Mathf.Atan2(-Mathf.Sqrt(1 - Mathf.Pow((p.x * (l1 + l2 * Mathf.Cos(y_option_1) + p.y * l2 * Mathf.Sin(y_option_1))) / (p.x * p.x + p.y * p.y), 2)),
                                    (p.x * (l1 + l2 * Mathf.Cos(y_option_1) + p.y * l2 * Mathf.Sin(y_option_1))) / (p.x * p.x + p.y * p.y));

            return new Vector2(y_option_1, x_option);
        }
        else
        {
            float x_option = Mathf.Atan2(Mathf.Sqrt(1 - Mathf.Pow((p.x * (l1 + l2 * Mathf.Cos(y_option_2) + p.y * l2 * Mathf.Sin(y_option_2))) / (p.x * p.x + p.y * p.y), 2)),
                        (p.x * (l1 + l2 * Mathf.Cos(y_option_2) + p.y * l2 * Mathf.Sin(y_option_2))) / (p.x * p.x + p.y * p.y));
            if (!float.IsNaN(x_option))
                return new Vector2(y_option_1, x_option);

            x_option = Mathf.Atan2(-Mathf.Sqrt(1 - Mathf.Pow((p.x * (l1 + l2 * Mathf.Cos(y_option_2) + p.y * l2 * Mathf.Sin(y_option_2))) / (p.x * p.x + p.y * p.y), 2)),
                                    (p.x * (l1 + l2 * Mathf.Cos(y_option_2) + p.y * l2 * Mathf.Sin(y_option_2))) / (p.x * p.x + p.y * p.y));

            return new Vector2(y_option_2, x_option);
        }
        */
    }
}

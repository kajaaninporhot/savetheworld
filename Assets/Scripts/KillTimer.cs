﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

[RequireComponent(typeof(ParticleSystem))]
public class KillTimer : MonoBehaviour, TimedBlock
{
    ParticleSystem system;

    public float first_phase_time = 5;
    public float second_phase_time = 10;

    Timer first_time;
    Timer second_time;

	// Use this for initialization
	void Start ()
    {
        system = GetComponent<ParticleSystem>();

        first_time = new Timer(0, first_phase_time, false, this);
        second_time = new Timer(1, second_phase_time, false, this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        first_time.update();
        second_time.update();
	}

    public void timeIsUp(int id)
    {
        switch (id)
        {
            case 0:
                var e = system.emission;
                e.enabled = false;
                break;
            case 1:
                Destroy(gameObject);
                break;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class TriggerSeparator : MonoBehaviour
{
    Rigidbody2D body;
    FixedJoint2D joint;

	// Use this for initialization
	void Start ()
    {
        joint = GetComponent<FixedJoint2D>();
        body = GetComponent<Rigidbody2D>();
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        //transform.gameObject.layer = 0;
        Debug.Log("Hat hit");
        body.mass = 2;
        Destroy(joint);
    }
}

﻿using UnityEngine;
using System.Collections;

public class quit : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
